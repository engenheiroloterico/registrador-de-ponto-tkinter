from tkinter import Button, Entry, Label, PhotoImage, Tk
from  tkinter import ttk 
import datetime as dt
import sqlite3

con = sqlite3.connect('registro_pronto.db')
cur = con.cursor()

cur.execute('''CREATE TABLE IF NOT EXISTS ponto
       (Colaborador, Entrada, Saida)''')

#cur.execute('DROP TABLE ponto')
def current_time():
    now = dt.datetime.now()
    date_actual = dt.datetime(day=now.day, month=now.month,year=now.year, hour=now.hour, minute=now.minute, second=now.second)
    return date_actual

def enter():
    get_name = check_name.get().title().strip()
    cur.execute(f'INSERT INTO ponto VALUES ("{get_name}","{current_time()}","")')
    show_row_cad()
    con.commit()
    check_name.delete(0, len(check_name.get()))
    
def out():
    cur.execute(f'UPDATE ponto SET Saida="{current_time()}" WHERE Colaborador="{check_name.get().title().strip()}"')
    con.commit()

    for row in table.get_children():
        table.delete(row)
    show_table()

def update_record():
    selected = table.focus()
    row = cur.execute(f'SELECT * FROM ponto WHERE Colaborador="{check_name.get().title().strip()}"') # filtra linha
    content = row.fetchall()
    table.insert(parent='',index='end',iid=None,text='',values=(content[0]))
    table.item(selected, text="",values=(content[0])) # atualiza tabela

# -------------------------- TABELA -------------------------------------------
window = Tk()
style = ttk.Style(window)

style.configure("Treeview",
                    background="coral",
                    foreground="black",
                    rowheight=25,
                    font=('bold'),
                    )

game_frame = ttk.Frame(window, border=2, borderwidth=5)
game_frame.grid(row=0, columns=1, columnspan=3)

game_scroll = ttk.Scrollbar(game_frame)
game_scroll.grid(row=0, column=2)

table = ttk.Treeview(game_frame,yscrollcommand=game_scroll.set, xscrollcommand =game_scroll.set, padding=5, selectmode='browse')
table.grid(row=0, columns=1)

game_scroll.config(command=table.yview)

table['columns'] = ('col_name', 'col_in', 'col_out')

#COLUNAS
table.column("#0",width=1, stretch=True)

# LINHAS
table.heading("#0",text="",anchor='center')
table.heading("col_name",text='Colaborador',anchor='center')
table.heading("col_in",text="Entrada",anchor='center')
table.heading("col_out",text="Saída",anchor='center')

# EXIBE O ITEM PESQUISADO
def show_row_cad():
    row = cur.execute(f'SELECT * FROM ponto WHERE Colaborador="{check_name.get().title()}"')
    for content in row.fetchall():
        table.insert(parent='',index='end',iid=None,text='',values=(content))
    check_name.delete(0, len(check_name.get()))

def select_name():
    query = check_name.get().title()
    selections = []
    for child in table.get_children():
        if query in table.item(child)['values']:   # compare strings in  lower cases.
            selections.append(child)
    table.selection_set(selections)

#MOSTRA A TABELA
def show_table():
    row = cur.execute(f'SELECT * FROM ponto')
    for content in row.fetchall():
        table.insert(parent='',index=-1,iid=None,text='',values=(content))

# ----------------------------------------------------------------------------------------
window.title('Registrador de Ponto')
window.minsize()
window.config(padx=10, pady=10, bg='coral')

label = Label(text='Primeiro nome e Registro:', bg='coral', fg='black', font=('Arial', 15,'bold'))
label.grid(row=1,column=0, columnspan=1)

check_name = Entry(width=40)
check_name.grid(row=1,column=1)

# REGISTRA ENTRADA
img_check_in = PhotoImage(file='images\REGISTRAR ENTRADA.png')
check_in = Button(image=img_check_in, command=enter)
check_in.grid(row=2, column=0)

# REGISTRA SAIDA
img_check_out = PhotoImage(file='images\REGISTRAR SAIDA.png')
check_out = Button(image=img_check_out, command=out)
check_out.grid(row=2, column=1)

#PESQUISA
search_name = Button(text='Procurar', width=14, command=select_name, bg='black', fg='white', font=('Arial',7,'bold'))
search_name.grid(row=1, column=2)

show_table()
window.mainloop()
